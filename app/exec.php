<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 6/9/13
 * Time: 3:31 AM
 * To change this template use File | Settings | File Templates.
 */
$yashr_core="../yashr/yashr.php";

require_once($yashr_core);

try
{
    Yashr::main(array(
        'configs'   => require_once('configs.php'),
        'yashr'     => "../yashr",
        'app'       => dirname(dirname($_SERVER['DOCUMENT_ROOT'])),
    ));
}
catch (\Yashr\Classes\YashrException $e)
{
    $e->view_error();
}
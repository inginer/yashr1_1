<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */

if (strpos(getcwd(),"dev"))
{
    $db_user = 'root';
    $db_pass = 'root';
    $db_name = 'ncom';
    $host = 'yashr.app';
}
else
{
    $db_user = 'root';
    $db_pass = 'MalinaMS';
    $db_name = 'malina_bb';
    $host    = 'n-com.uz';
}

return array(
    'db' => array(
        'host' => 'localhost',
        'user' => $db_user,
        'pass' => $db_pass,
        'dbname' => $db_name,
        'charset' => 'utf8',
        'timezone' => '+5:00',
        'tables_statistics' => array(
            'go' => true,
            'table' => 'yashr_model',
        ),
        'sqlite' => array(
            'file' => ROOT.'/logs/db/sqlite.db',
            'mode' => 0666,
            'charset' => 'utf-8'
        ),
        'mongo' => array(
            'host'  => 'localhost',
            'port'  => 27017,
            'dbname' => 'yashr',
        ),
    ),
    'mem' => array(
        'host' => 'localhost',
        'port' => 11211,
        'prefix' => 'debug_',
        'servers' => array(
            array(
                'connect' => true,
                'host' => 'localhost',
                'port' => 11211,
            ),
            array(
                'connect' => false,
                'host' => 'localhost',
                'port' => 11211,
            ),
        )
    ),
    'email'     => array(
        'user'      => 'demon',
        'table'     => 'email',
        'template'  => 'default',
    ),
    'error'     => array(
        'table'     => 'error',
        'level'     => E_ALL,
        'template'  => 'default',
    ),
    'namespace' => '\\App\\Controllers\\',
    'hash'      => '69dab461d1b77e317c4be3a45d154dbc',
    'cookie'    => array(
        'time' => 86400,
        'path' => '/',
    ),
    'file' => array(
        'path'  => '/up',
        'ext'   => array(
            'jpg',
            'jpeg',
            'png',
            'gif',
            'xlsx',
        ),
        'max_size' => 20000000,
        'table' => 'file',
    ),
    'access_ip' => array(
        '192.168.253.1',
        '127.0.0.1',
        '*',
        '192.168.2.109',
        '192.168.2.105',
    ),
    'debug' => true,
    'host'  => $host,
    'pagination' => array(
        'switchboard' => 20,
    ),
    'template'  => 'metro',
    'templates' => array(
        'default' => array('path' => '',),
        'metro'   => array('path'=>'/metro','catch' => array(
            'switchboard',
        )),
    ),
);
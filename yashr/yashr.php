<?php
use Yashr\Classes\Core;

/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */

include_once('classes/core.php');

/**
 * Class Yashr
 */
final class Yashr
{
    /**
     * Yashr version
     */
    const YASHR_VERSION     = '1.2.0';

    /**
     * PHP minimum version
     */
    const PHP_MIN_VERSION   = '5.3';

    /**
     * MySQL minimum version
     */
    const MYSQL_MIN_VERSION = '5.5';

    /**
     * Mongo minimum version
     */
    const MONGO_MIN_VERSION = '2.0.4';

    /**
     * @param array $params
     * Yashr constructor
     */
    final public static function main (array $params=array())
    {
        /*
         * Функция __autoload ну это всем понятно
         * */
        function __autoload ($class_name)
        {
            include_once(Core::find_class_file($class_name));
        }

        Core::$configs  = array_merge(require_once('configs.php'),$params['configs']);
        Core::$yashr    = $params['yashr'];
        Core::$app      = $params['app'];

        /*
         * Пишем свой отладчик ошибок
         * */
        if (Core::$configs['debug'])
        {
            function handler($errno, $error, $error_file, $error_line)
            {
                Core::error_handler($errno, $error, $error_file, $error_line);
            }
            set_error_handler("handler",Core::$configs['error']['level']);
        }

        /*
         * Указываем уровен ошибок
         * */
        error_reporting(Core::$configs['error']['level']);


        /*
         * Установка TIMEZONE
         * */
        date_default_timezone_set('Europe/Moscow');

        /*
         * Заупскаем сессию
         * */
        if (!empty(Core::$configs['session']))
        {
            session_start();
        }

        /*
         * Если наша прилодения не демон тогда запускаем Роутер
         * */
        if (!defined('DEMON'))
        {
            $route = new \Yashr\Classes\Route();
            $route->exec();
            $route->shutdown();
        }
        else
        {
            if (!empty($params['demon_name']))
            {
                $demon=$params['demon_name'];
                require_once(Core::$yashr.'yashr/bin/demon.php');
            }
            $params['demon']();
        }
    }
}
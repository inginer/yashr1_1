<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:38
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Classes;


/**
 * Class Controller
 * @package Yashr\Classes
 */
abstract class Controller extends Core
{
    /**
     * @var string
     */
    public static $_return  = '';

    /**
     * @var bool
     */
    public static $_display = true;

    /**
     * @var string
     */
    public static $_layout  = 'main';

    /**
     * @var array
     */
    public static $_assign  = array();


    /**
     * @param array $columns
     * @param array $data
     * @return array
     */
    public function filter_data (array $columns=array(), array $data=array())
    {
        $array=array();

        foreach ($columns as $col)
        {
            $array[$col]=(isset($data[$col]) ? $data[$col] : NULL);
        }
        unset($data,$columns);

        return $array;
    }

    /**
     * Информация о таблицах
     */
    public function yashr_info()
    {
        $this->yashr_table_statistic('controller');
    }

    /**
     * @return mixed
     */
    abstract public function index ();
}
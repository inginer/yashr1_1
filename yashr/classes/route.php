<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Classes;

Core::import('yashr.drivers.view');

/**
 * Class Route
 * @package Yashr\Classes
 */
final class Route extends Core
{
    /**
     * @var string
     */
    public static $url              = '';

    /**
     * @var string
     */
    public static $controller       = 'Main';

    /**
     * @var string
     */
    public static $method           = 'index';

    /**
     * @var array
     */
    public static $params           = array();

    /**
     * @var array
     */
    public static $namespace        = array();

    /**
     * @var string
     */
    public static $query            = '';

    /**
     * @var array
     */
    public static $headers          = array();

    /**
     * @var bool
     */
    public static $is_ajax          = false;

    /**
     * @var null
     */
    public static $ip               = NULL;

    /**
     * @var string
     */
    public static $sub_app          = '';

    /**
     * @var string
     */
    private static $type            = 'html';

    /**
     * @var string
     */
    private static $template        = '';

    /**
     * @var string
     */
    private static $layout          = '';

    /**
     * @var string
     */
    private static $request_method  ='';

    public static $sub_domain = '';

    public static $method_redirect = array();

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $url = parse_url($_SERVER['REQUEST_URI']);

        self::$url = explode('/',$url['path']);

        if (isset($url['query']))
        {
            self::$query = $url['query'];
        }

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            self::$is_ajax = true;
        }

        self::$ip               = $_SERVER['REMOTE_ADDR'];
        self::$request_method   = $_SERVER['REQUEST_METHOD'];
        self::$headers          = getallheaders();

        $host=explode('.',str_replace(self::$configs['host'],'',$_SERVER['HTTP_HOST']));

        if (!empty($host[0]) && $host[0]!='www')
        {
            self::$sub_domain=$host[0];
        }

        if (!empty(self::$configs['method_redirect']))
        {
            self::$method_redirect=array_merge(self::$method_redirect,self::$configs['method_redirect']);
        }

        if (self::$configs['debug'] &&
            (
                !in_array(self::$ip, self::$configs['access_ip']) &&
                !in_array('*', self::$configs['access_ip'])
            )
        )
        {
            die('Access deinded! For IP: <b>'.self::$ip.'</b>');
        }

        if (!empty(self::$url[1]))
        {
            self::$controller = ucfirst(self::$url[1]);
        }

        if (!empty(self::$url[2]))
        {
            self::$method = self::$url[2];

            if (!empty(self::$method_redirect[strtolower(self::$controller.'_'.self::$url[2])]))
            {
                $redirect_method=self::$method_redirect[strtolower(self::$controller.'_'.self::$url[2])];

                self::$method       = $redirect_method['method'];
                self::$controller   = $redirect_method['controller'];
            }
        }

        if (!empty(self::$url[3]))
        {
            self::$params = array_slice(self::$url,3);
        }

        self::$namespace = self::$configs['namespace'];

        if (!self::$sub_domain)
        {
            $path = static::$app.str_replace('\\','/',strtolower(self::$namespace.self::$controller));
        }
        else
        {
            $path = static::$app.str_replace('\\','/',strtolower(self::$namespace.self::$sub_domain.'/'.self::$controller));
            self::$controller = 'main';
            self::$method = 'index';
            self::$params = array();

            if (!empty(self::$url[1]))
            {
                self::$controller = ucfirst(self::$url[1]);
            }

            if (!empty(self::$url[2]))
            {
                self::$method = self::$url[2];
            }

            if (!empty(self::$url[3]))
            {
                self::$params = array_slice(self::$url,3);
            }

            self::$namespace = self::$configs['namespace'].ucfirst(self::$sub_domain).'\\';

            if (
                !empty(self::$configs['templates'][self::$configs['template']]['catch']) &&
                in_array(self::$sub_domain,self::$configs['templates'][self::$configs['template']]['catch'])
            )
            {
                $this->view->set_template_path('default');
            }
//            self::$sub_app = self::$url[1].'/';
        }

        if (is_dir($path))
        {

            self::$controller = 'main';
            self::$method = 'index';
            self::$params = array();

            if (!empty(self::$url[2]))
            {
                self::$controller = ucfirst(self::$url[2]);
            }

            if (!empty(self::$url[3]))
            {
                self::$method = self::$url[3];
            }

            if (!empty(self::$url[4]))
            {
                self::$params = array_slice(self::$url,4);
            }

            self::$namespace = self::$configs['namespace'].ucfirst(self::$url[1]).'\\';

            if (
                !empty(self::$configs['templates'][self::$configs['template']]['catch']) &&
                in_array(self::$url[1],self::$configs['templates'][self::$configs['template']]['catch'])
            )
            {
                $this->view->set_template_path('default');
            }
            self::$sub_app = self::$url[1].'/';
        }

    }

    /**
     * @throws YashrException
     */
    public function shutdown ()
    {

        if (!class_exists(self::$namespace.self::$controller))
        {
            throw new YashrException('Controller with name ' . self::$controller . ' not found!');
        }

        $class = new \ReflectionClass(self::$namespace.self::$controller);

        if ($class->isAbstract())
        {
            throw new YashrException('Controller with name ' . self::$controller . ' not found!');
        }

        $object = $class->newInstance();

        if (!$class->hasMethod('exec'))
        {
            throw new YashrException('In the controller ' . self::$controller . ' not found method exec!');
        }

        if (!$class->hasMethod(self::$method))
        {
            throw new YashrException('In the controller ' . self::$controller . ' not found method '.self::$method.'!');
        }

        if (!($object instanceof Controller))
        {
            throw new YashrException('Unknown controller');
        }

        $exec = $class->getMethod('exec');
        $exec->invoke($object);

        $method = $class->getMethod(self::$method);
        $res = $method->invokeArgs($object, self::$params);

        self::$type = ($class->getStaticPropertyValue('_return') ? $class->getStaticPropertyValue('_return') : self::$type);

        if ($class->getStaticPropertyValue('_display') && self::$type == 'html')
        {
            if (is_string($class->getStaticPropertyValue('_display')))
            {
                self::$template = $class->getStaticPropertyValue('_display');
            }
            else
            {
                self::$template = strtolower(strtolower(self::$sub_app.self::$controller.'/'.self::$method));
            }
            self::$layout = $class->getStaticPropertyValue('_layout');
        }

        self::display($res);
    }

    /**
     * @param array $data
     */
    private static function display ( $data = array() )
    {
        if (!is_array($data))
        {
            $data = array();
        }

        switch ( self::$type )
        {
            case "html":
            {
                if (self::$template)
                {
                    $layout = false;
                    if (self::$layout)
                    {
                        Core::call('view')->set_layout('layout/'.self::$layout);
                    }
                    else
                    {
                        $layout = true;
                    }
                    echo(Core::call('view')->display(self::$template, $data,$layout));
                }
                break;
            }
            case "json":
            {
                die(json_encode($data));
                break;
            }
            case "xml":
            {
                die(json_encode($data));
                break;
            }
            default:
            {
                die('Undefined type for display!');
                break;
            }
        }
    }
}
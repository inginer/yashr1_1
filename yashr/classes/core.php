<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Classes;

Core::import('yashr.drivers.view');

/**
 * Class Core
 * @package Yashr\Classes
 */
abstract class Core
{
    /**
     * @var string
     */
    static public $yashr = '';

    /**
     * @var string
     */
    static public $app   = '';

    /**
     * @var string
     */
    static public $ext   = '.php';

    /**
     * @var array
     */
    static private  $imported   = array();

    /**
     * @var array
     */
    static private  $called     = array();

    /**
     * @var array
     */
    static public   $configs    = array();

    /**
     * @var null
     */
    static public $object_start_time = null;

    /**
     * @param array $params
     * @return mixed
     */
    abstract public function exec (array $params = array());

    /**
     * @param null $class
     * @return string
     * @throws YashrException
     */
    public static function find_class_file ( $class=NULL )
    {
        if (is_null($class))
        {
            throw new YashrException('Class name can\'t be empty');
        }

        $class  = strtolower($class);
        $class  = str_replace("\\","/",$class).self::$ext;
        $file   = self::$yashr.$class;

        if (!is_file($file))
        {
            $file = self::$app.$class;
        }


        if (!file_exists($file))
        {
            throw new YashrException('File not found! ' . $file);
        }

        return $file;
    }

    /**
     * @param $name
     * @param null $alias
     */
    public static function import ($name, $alias=NULL)
    {
        $class_name = str_replace('.', '\\', $name);

        $explode = explode(".", $name);

        $a = end($explode);

        if (!is_null($alias))
        {
            $a = $alias;
        }

        if (!isset(self::$imported[$a]))
        {
            self::$imported[$a] = $class_name;
        }
    }

    /**
     * @param $name
     * @return mixed
     * @throws YashrException
     */
    public static function call ($name)
    {
        if (!isset(self::$imported[$name]))
        {
            throw new YashrException('Called undefined object with name ' . $name);
        }

        if (!isset(self::$called[$name]))
        {
            self::$object_start_time = microtime(true);
            self::$called[$name] = new self::$imported[$name]();
            self::$called[$name]->exec();
        }

        if(self::$configs['db']['tables_statistics']['go'] && in_array('Yashr\Classes\Model',class_parents(self::$called[$name])))
        {
            self::$called[$name]->yashr_info();
        }

        return self::$called[$name];
    }

    /**
     * @param $errno
     * @param $error
     * @param $error_file
     * @param $error_line
     */
    public static function error_handler ($errno, $error, $error_file, $error_line)
    {
        $params = array(
            'errno'         => $errno,
            'error'         => $error,
            'error_file'    => $error_file,
            'error_line'    => $error_line,
        );

        YashrException::add($params);
        die(Core::call('view')->display('error/'.Core::$configs['error']['template'], $params, true));
    }

    /**
     * @param $type
     */
    public function yashr_table_statistic ($type)
    {
        $this->create_static_table();
        switch($type)
        {
            case "model":
            {
                $table = $this->mysqli->escape($this->table());
                $rows = $this->mysqli->rows("SELECT 1 FROM " . self::$configs['db']['tables_statistics']['table'] . " WHERE `table`=" . $table);
                if($rows)
                {
                    $sql = "UPDATE
                        " . self::$configs['db']['tables_statistics']['table'] . "
                    SET
                        `count`=`count`+1,
                        memory=" . $this->mysqli->escape(round(memory_get_peak_usage()/(1024), 2)).",
                        last_query=NOW(),
                        time=".$this->mysqli->escape(microtime(true)-self::$object_start_time)."

                    WHERE
                        `table`=".$table."
                    LIMIT 1";
                }
                else
                {
                    $sql = "INSERT INTO
                        " . self::$configs['db']['tables_statistics']['table'] . "
                    SET
                        `count`=1,
                        memory=" . $this->mysqli->escape(round(memory_get_peak_usage()/(1024), 2)).",
                        last_query=NOW(),
                        `table`=".$table.",
                        `type`='".$type."',
                        time=".$this->mysqli->escape(microtime(true)-self::$object_start_time);
                }

                $this->mysqli->q($sql);
                break;
            }
        }

    }

    /**
     *
     */
    private function create_static_table()
    {
        $res = $this->mysqli->q("SHOW TABLES LIKE '" . self::$configs['db']['tables_statistics']['table'] . "'");

        if (!$this->mysqli->rows($res))
        {
            $sql = "CREATE TABLE " . self::$configs['db']['tables_statistics']['table'] . " (
                        `table` CHAR(50) NOT NULL COLLATE 'utf8_bin',
                        `count` INT(11) NOT NULL,
                        `memory` CHAR(50) NOT NULL COLLATE 'utf8_bin',
                        `last_query` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        `time` CHAR(50) NOT NULL COLLATE 'utf8_bin',
                        `type` ENUM('model','controller') NOT NULL
                    )
                    COLLATE='utf8_bin'
                    ENGINE=InnoDB;";

            $this->mysqli->q($sql);
        }
    }

    /**
     * @param array $data
     * @param string $type
     * @param bool $die
     */
    public static function debug ($data=array(), $type='array', $die=true)
    {
        switch($type)
        {
            case "array":
            {
                echo ('<pre>');
                print_r($data);
                echo ('</pre>');
                break;
            }
            case "json":
            {
                echo json_encode($data);
                break;
            }
        }

        if ($die)
        {
            die;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get ($name)
    {
        return self::call($name);
    }
}
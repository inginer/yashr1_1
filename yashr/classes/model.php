<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Classes;

Core::import('yashr.drivers.db.mysqli');

/**
 * Class Model
 * @package Yashr\Classes
 */
abstract class Model extends Core
{
    /**
     * @var array
     * @deprecated
     */
    protected $default = array(
        'fetch' => '',
    );

    /**
     * @return mixed
     */
    abstract public function table ();

    /**
     * @param array $params
     * @return mixed
     * @deprecated
     */
    final public function by (array $params = array())
    {
        $params = array_merge($this->default,$params);
        $order=" ORDER BY id DESC ";
        $where = '';

        foreach ($params as $key => $val)
        {
            if ($val && $key!='fetch')
            {
                $where .= ' AND `' . $key . '`=' . $this->mysqli->escape($val);
            }
        }

        $sql = "SELECT * FROM " . $this->table() . " WHERE 1=1 " . $where . $order;
        if (!$params['fetch'])
        {
            $res=$this->mysqli->fetch($sql);
        }
        else
        {
            $res=$this->mysqli->fetch_all($sql);
        }
        return $res;
    }

    /**
     * @param array $params
     * @param bool $table
     * @return mixed
     */
    protected function insert (array $params = array(), $table=false)
    {
        $t=$this->table();
        if ($table)
        {
            $t=$table;
        }
        $keys = $vals = '';

        foreach ($params as $key => $val)
        {
            $keys .= '`' . $key . '`,';
            $vals .= $this->mysqli->escape($val).",";
        }
        $vals = substr($vals,0,-1);
        $keys = substr($keys,0,-1);

        $this->mysqli->q("INSERT INTO " . $t . " (" . $keys . ") VALUES (" . $vals . ")");

        return $this->mysqli->last_id();
    }

    /**
     * @param array $data
     * @param array $params
     * @return mixed
     */
    protected function update (array $data = array(), array $params = array())
    {
        $where = "";
        $data_string = "";

        foreach ($data as $key => $val)
        {
            $data_string .= '`' . $key . '`='. $this->mysqli->escape($val).",";
        }

        foreach ($params as $k => $v)
        {
            $where .= ' AND `' . $k . '`='. $this->mysqli->escape($v);
        }

        $data_string=substr($data_string,0,-1);

        $sql = "UPDATE `" . $this->table() . "` SET " . $data_string . " WHERE 1=1 " . $where . " LIMIT 1";

        return $this->mysqli->q($sql);
    }

    /**
     * @param array $params
     * @return string
     */
    private function select_sql (array $params)
    {
        $default=array(
            'order' => 1,
            'where' => '',
            'desc'  => '',
            'limit' => 0,
        );

        $params=array_merge($default,$params);
        $where="";

        foreach ($params as $key => $param)
        {
            $where.=" AND `".$key."`=".$param;
        }

        $sql="SELECT
                * FROM " . $this->table() . "
              WHERE
                1=1 " . $where . "
              ORDER BY
                " . $params['order'] . " " . strtoupper($params['desc']) . ($params['limit'] ? " LIMIT " . $params['limit'] : '');

        return $sql;
    }

    /**
     * @param array $params
     * @return mixed
     */
    final protected function select (array $params=array())
    {
        $sql=$this->select_sql($params);

        return $this->mysqli->fetch($sql);
    }

    /**
     * @param array $params
     * @return mixed
     */
    final protected function select_all (array $params=array())
    {
        $sql=$this->select_sql($params);

        return $this->mysqli->fetch_all($sql);
    }
    /**
     *
     */
    public function yashr_info ()
    {
        $this->yashr_table_statistic('model');
    }
}
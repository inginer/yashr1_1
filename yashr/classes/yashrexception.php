<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Classes;

Core::import('yashr.drivers.db.mysqli');

/**
 * Class YashrException
 * @package Yashr\Classes
 */
class YashrException extends \Exception
{
    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);

        static::add(array(
            'error'         => static::getMessage(),
            'errno'         => static::getCode(),
            'error_file'    => static::getFile(),
            'error_line'    => static::getline(),
        ));
    }

    public function view_error ()
    {
        Core::error_handler(static::getCode(),static::getMessage(),static::getFile(),static::getLine());
    }

    /**
     * @param array $params
     */
    public static function add (array $params = array())
    {
        $default=array(
            'error' => 'Unknown error!',
            'errno' => 0,
            'error_file' => 'none',
            'error_line' => 0,
        );
        $params=array_merge($default,$params);
        $db = Core::call('mysqli');
        static::create_table();
        $sql = "INSERT INTO `".Core::$configs['error']['table']."`
                    (`name`,`code`,`file`,`line`,dt,`type`,ip)
                        VALUES
                        (
                        ".$db->escape($params['error']).",
                        ".$db->escape($params['errno']).",
                        ".$db->escape($params['error_file']).",
                        ".$db->escape($params['error_line']).",
                        NOW(),
                        'error',
                        ".$db->escape((!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1'))."
                        )";
        $db->q($sql);
    }

    /**
     *
     */
    private static function create_table ()
    {
        $res = Core::call('mysqli')->q("SHOW TABLES LIKE '" . Core::$configs['error']['table'] . "'");
        if (!Core::call('mysqli')->rows($res))
        {
            $sql = "CREATE TABLE `".Core::$configs['error']['table']."` (
                        `id` INT(10) NOT NULL AUTO_INCREMENT,
                        `name` VARCHAR(655) NOT NULL COLLATE 'utf8_bin',
                        `code` VARCHAR(50) NOT NULL COLLATE 'utf8_bin',
                        `file` VARCHAR(655) NOT NULL COLLATE 'utf8_bin',
                        `line` VARCHAR(655) NOT NULL COLLATE 'utf8_bin',
                        `dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `type` VARCHAR(20) NOT NULL COLLATE 'utf8_bin',
                        PRIMARY KEY (`id`)
                    )
                    COLLATE='utf8_bin'
                    ENGINE=InnoDB";

            Core::call('mysqli')->q($sql);
        }
    }
}
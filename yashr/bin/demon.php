<?php
/**
 * Created by JetBrains PhpStorm.
 * User: twocomrades (ats24)
 * Date: 29.04.13
 * Time: 1:11
 */

/**
 * Для работы необходимо создать директории pid и log с правами 0777
 */
if (empty($demon))
{
    echo "$demon variable should be defined\n";
    exit;
}
define('ROOT', \Yashr\Classes\Core::$yashr);

$stop=false;

$pidfile=ROOT.'yashr/logs/pid/'.$demon.'.pid';

ini_set('error_log', ROOT.'yashr/logs/log/'.$demon.'-error.log');

fclose(STDIN);

fclose(STDOUT);

fclose(STDERR);
$STDIN = fopen('/dev/null', 'r');

$STDOUT = fopen(ROOT.'yashr/logs/log/'.$demon.'-application.log', 'ab');

if (isset($_SERVER['argv'][1]) && $_SERVER['argv'][1]=='restart')
{
    system('kill -SIGTERM '.file_get_contents($pidfile));
    system(__FILE__.' > /dev/null &');
    exit;
}

// создаем дочерний процесс
$child_pid=pcntl_fork();
// выходим из родительского, привязанного к консоли, процесса
if($child_pid)
{
    exit;
}

// делаем основным процессом дочерний.
posix_setsid();
/**
 * @param $pid_file
 * @return bool
 */
function isDaemonActive($pid_file)
{
    if(is_file($pid_file))
    {
        $pid=file_get_contents($pid_file);
        //проверяем на наличие процесса
        if(posix_kill($pid,0))
        {
            //демон уже запущен
            return true;
        }
        else
        {
            //pid-файл есть, но процесса нет
            if(!unlink($pid_file))
            {
                //не могу уничтожить pid-файл. ошибка
                exit(-1);
            }
        }
    }
    return false;
}

if (isDaemonActive($pidfile))
{
    //echo 'Daemon already active';
    exit;
}

file_put_contents($pidfile, getmypid());


/**
 * Обработка сигналов ######################################################
 */

//Без этой директивы PHP не будет перехватывать сигналы
declare(ticks=1);

/**
 * @param $signo
 */
function sig_handler($signo)
{
    global $stop;
    switch($signo)
    {
        case SIGTERM:
        {
            $stop=true;
            break;
        }
        default:
            {
            //все остальные сигналы
            }
    }
}
//регистрируем обработчик
pcntl_signal(SIGTERM, "sig_handler");
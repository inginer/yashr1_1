<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 22.09.13
 * Time: 3:20
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\File;


use Yashr\Classes\Driver;

/**
 * Class File
 * @package Yashr\Drivers\File
 */
abstract class File extends Driver
{
    /**
     * @var null
     */
    protected $file=null;
    /**
     * @var null
     */
    protected $path=null;

    /**
     * @param $filename
     * @return mixed
     */
    abstract public function open ( $filename );

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function view (array $data=array());

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function write (array $data=array());

    /**
     *
     */
    public function rm ()
    {
        @unlink($this->path.'/'.$this->file);
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 2:46
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers;


use Yashr\Classes\Driver;

/**
 * Class Datetime
 * @package Yashr\Drivers
 */
class Datetime extends Driver
{
    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
    }

    /**
     * @var array
     */
    public static $month =	array(
        'января',
        'февраля',
        'марта',
        'апреля',
        'мая',
        'июня',
        'июля',
        'августа',
        'сентября',
        'октября',
        'ноября',
        'декабря'
    );

    /**
     * @var array
     */
    public static $short_month =	array(
        'янв',
        'фев',
        'мар',
        'апр',
        'май',
        'июн',
        'июл',
        'авг',
        'сен',
        'окт',
        'ноя',
        'дек'
    );

    /**
     * @param array $data
     * @return bool|string
     */
    public function current (array $data = array())
    {
        $default = array(
            'type' => 'dt'
        );

        $data = array_merge($default,$data);

        switch ($data['type'])
        {
            case "dt":
            default:
            {
                $date = date('Y:m:d H:i:s');
                break;
            }
            case "time":
            {
                $date = date('H:i:s');
                break;
            }
            case "date":
            {
                $date = date('Y:m:d');
                break;
            }
        }

        return $date;
    }

    /**
     * @param array $data
     * @return string
     */
    public function format (array $data = array())
    {
        $default = array(
            'human'	=> 1,
            'date'	=> '',
            'type'	=> ''
        );
        $data = array_merge($default, $data);
        $date = $data['date'];
        $end='';

        if (!is_numeric($date))
        {
            $date=strtotime($date);
        }

        if (!$data['human'])
        {
            if (date('y', $date)==date('y'))
            {
                $end.=date('d', $date).' '.self::$month[date('n', $date)-1];
            }
            else
            {
                $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
            }
            $end.=' в '.date('H:i', $date);
            return $end;
        }

        switch ($data['type'])
        {
            case 'time':
            {
                $end.=date('H:i', $date);
                break;
            }
            case 'number':
            {
                if (date('y', $date)==date('y'))
                {
                    $end.=date('d', $date).' '.self::$month[date('n', $date)-1];
                }
                else
                {
                    $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
                }
                break;
            }
            case 'date':
            {
                if (date('dmy', $date)==date('dmy'))
                {
                    $end.='';
                }
                elseif (date('y', $date)==date('y'))
                {
                    $end.=date('d', $date).' '.self::$month[date('n', $date)-1];
                }
                else
                {
                    $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
                }
                break;
            }
            case 'fulldate':
            {
                $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
                break;
            }
            case 'shortdate':
            {
                if (date('dmy', $date)==date('dmy'))
                {
                    $end.=date('H:i', $date);
                }
                else
                {
                    if (!empty($data['time']))
                    {
                        $end.=date('d.m H:i', $date);
                    }
                    else
                    {
                        $end.=date('d', $date).' '.self::$short_month[date('n', $date)-1];
                    }
                }
                break;
            }
            case 'docdate':
            {
                if (date('dmy', $date) == date('dmy'))
                {
                    $end .= date('H:i', $date);
                }
                else
                {
                    $end .= date('d', $date).'.'.date('m', $date);
                    $end .= ' '.date('H:i', $date);
                }
                break;
            }
            case 'all':
            {
                if (date('dmy', $date)==date('dmy'))
                {
                    $end.= date('H:i', $date);
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')+1, date('y'))))
                {
                    $end.=self::t('завтра');
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')-1, date('y'))))
                {
                    $end.=self::t('вчера');
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')+2, date('y'))))
                {
                    $end.=self::t('послезавтра');
                }
                else
                {
                    if (date('y', $date)==date('y'))
                    {
                        $end.=date('d', $date).' '.self::$month[date('n', $date)-1];
                    }
                    else
                    {
                        $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
                    }
                }
                break;
            }
            default:
                {
                if (date('dmy', $date)==date('dmy'))
                {
                    $end.=self::t('сегодня');
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')+1, date('y'))))
                {
                    $end.=self::t('завтра');
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')-1, date('y'))))
                {
                    $end.=self::t('вчера');
                }
                elseif (date('dmy', $date)==date('dmy', mktime(0, 0, 0, date('m'), date('d')+2, date('y'))))
                {
                    $end.=self::t('послезавтра');
                }
                else
                {
                    if (date('y', $date)==date('y'))
                    {
                        $end.=date('d', $date).' '.self::$month[date('n', $date)-1];
                    }
                    else
                    {
                        $end.=date('d', $date).' '.self::$month[date('n', $date)-1].' '.date('Y', $date);
                    }
                }
                $end.=' в '.date('H:i', $date);
                break;
                }
        }
        return $end;
    }

    /**
     * @param $str
     * @param array $values
     * @return mixed
     */
    public static function t ($str, $values=array())
    {
        return $str;
    }
}
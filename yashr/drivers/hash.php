<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 1:15
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers;


use Yashr\Classes\Driver;

/**
 * Class Hash
 * @package Yashr\Drivers
 */
class Hash extends Driver
{
    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {

    }

    /**
     * @param $text
     * @return string
     */
    public static function encode ( $text )
    {
        $iv = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);

        $iv2 = mcrypt_create_iv($iv, MCRYPT_RAND);

        $key = self::$configs['hash'];

        $text = base64_encode($text);

        return mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv2);
    }

    /**
     * @param $text
     * @return string
     */
    public static function decode ( $text )
    {
        $iv = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);

        $iv2 = mcrypt_create_iv($iv, MCRYPT_RAND);

        $key = self::$configs['hash'];

        $text =  mcrypt_decrypt (MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv2) ;

        return base64_decode( $text );
    }

    /**
     * @param $text
     * @return string
     */
    public function md5 ( $text )
    {
        return md5(self::$configs['hash'].$text);
    }
}
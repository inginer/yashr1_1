<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 6/1/13
 * Time: 2:58 AM
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers;


use Yashr\Classes\Core;
use Yashr\Classes\Driver;
use Yashr\Classes\YashrException;

Core::import('yashr.drivers.db.mysqli');
Core::import('yashr.drivers.session.session');

class Img extends Driver
{
    private $id = null;
    private $src = null;
    private $type = null;
/*
    private $format = array(
        'full',
        'medium',
        'min',
    );
*/
    private $dist = null;

    public function exec(array $params = array())
    {
    }

    public function get ( $id, $params = array())
    {
        $def = array(
            'rotate'    => 0,
            'format'    => 'full',
            'size'      => array(),
        );
        $params = array_merge($def,$params);

        switch ($params['format'])
        {
            case 'medium':
            {
                $params['size']['width'] = 300;
                $params['size']['height'] = 300;
                break;
            }
            case 'min':
            {
                $params['size']['width'] = 100;
                $params['size']['height'] = 100;
                break;
            }
        }

        $this->id = $id;

        $info = $this->info(array(
            'id' => $id,
        ));


        if (!$info['public'])
        {
            if (!$this->session->id)
            {
                exit;
            }
        }

        $this->src = $_SERVER['DOCUMENT_ROOT'].$info['path'].'/'.$info['title'];

        if (!is_file($this->src))
        {
            return '/img/no-photo.jpg';
            //throw new YashrException('File with name ' . $this->src . ' not found');
        }

        $this->type = substr($info['title'],-3,3);

        $this->create();

        if ($params['size'])
        {
            $this->dist = $this->resize($params['size']);
        }

        if ($params['rotate'])
        {
            $this->rotate($params['rotate']);
        }

        $this->img();
    }

    public function get2 ($id, $params = array())
    {
        $def = array(
            'rotate'    => 0,
            'format'    => 'full',
            'size'      => array(),
            'data'      => array(),
        );
        $params = array_merge($def,$params);

        switch ($params['format'])
        {
            case 'medium':
            {
                $params['size']['width'] = 300;
                $params['size']['height'] = 300;
                break;
            }
            case 'min':
            {
                $params['size']['width'] = 100;
                $params['size']['height'] = 100;
                break;
            }
        }

        $this->id = $id;

        $info = $params['data'];

        if (!$info['public'])
        {
            if (!$this->session->id)
            {
                exit;
            }
        }

        $this->src = $_SERVER['DOCUMENT_ROOT'].$info['path'].'/'.$info['title'];
        $file_name=$info['path'].'/'.$params['format'].'_'.$info['title'];
        $save_file=$_SERVER['DOCUMENT_ROOT'].$file_name;

        if (!file_exists($save_file))
        {
            if (!is_file($this->src))
            {
                return '/up/'.$params['format'].'-no-photo.jpg';//$this->src;
            }

            $this->type = substr($info['title'],-3,3);

            $this->create();

            if ($params['size'])
            {
                $this->dist = $this->resize($params['size'],$id);
            }

            if ($params['rotate'])
            {
                $this->rotate($params['rotate']);
            }



            $this->img($save_file);
        }

        return 'http://'.$info['host'].$file_name;
    }

    private function info (array $params = array())
    {
        $def = array(
            'id' => 0,
        );

        $params = array_merge($def,$params);
        $where = '';

        if ($params['id'])
        {
            $where .= ' AND id='.intval($params['id']);
        }

        return $this->mysqli->fetch("SELECT * FROM file WHERE 1=1 " . $where);
    }

    private function img ( $save=NULL )
    {
        header('Content-type: image/'.$this->type);
        switch ($this->type)
        {
            case "jpg":
            {
                imagejpeg($this->dist,$save,100);
                break;
            }
            case "png":
            {
                imagepng($this->dist,$save);
                break;
            }
        }

        imagedestroy($this->dist);
    }

    private function create ()
    {
        switch ($this->type)
        {
            case "jpg":
            {
                $this->dist = imagecreatefromjpeg($this->src);
                break;
            }
            case "png":
            {
                $this->dist = imagecreatefrompng($this->src);
                break;
            }
        }
    }

    private function rotate ( $degrees=90 )
    {
        $this->dist = imagerotate($this->dist, $degrees, 0);
    }

    private function resize (array $params = array())
    {
        $def = array(
            'width' => 0,
            'height' => 0,
        );
        $params = array_merge($def,$params);

        // задание максимальной ширины и высоты

        // тип содержимого
        header('Content-Type: image/'.$this->type);

        // получение новых размеров
        list($width_orig, $height_orig) = getimagesize($this->src);

        $ratio_orig = $width_orig/$height_orig;

        if ($params['width']/$params['height'] > $ratio_orig)
        {
            $params['width'] = $params['height']*$ratio_orig;
        }
        else
        {
            $params['height'] = $params['width']/$ratio_orig;
        }

        // ресэмплирование
        $tmp_photo = imagecreatetruecolor($params['width'], $params['height']);
        if ($this->type=='png')
        {
            imagealphablending($tmp_photo, false);
            imagesavealpha($tmp_photo, true);
        }

        imagecopyresampled($tmp_photo, $this->dist, 0, 0, 0, 0, $params['width'], $params['height'], $width_orig, $height_orig);

        return $tmp_photo;
    }
}
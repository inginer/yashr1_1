<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Upload;


use Yashr\Classes\Driver;

/**
 * Class INF
 * @package Yashr\Drivers\Upload
 */
abstract class INF extends Driver
{
    /**
     * @param array $file
     * @param array $return
     * @return mixed
     */
    abstract public function upload (array $file = array(), $return = array());
}
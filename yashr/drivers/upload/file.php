<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Upload;


use Yashr\Classes\Core;

Core::import('yashr.drivers.db.mysqli');
Core::import('yashr.drivers.session.session','sess');

/**
 * Class File
 * @package Yashr\Drivers\Upload
 */
class File extends INF
{
    /**
     * @var string
     */
    private $path = '';

    /**
     * @var null
     */
    private $ext_file = null;

    /**
     * @var null
     */
    private $size = null;

    /**
     * @var null
     */
    private $name = null;

    /**
     * @var null
     */
    private $tmp_name = null;

    /**
     * @var null
     */
    private $mime = null;

    /**
     * @var null
     */
    private $hash = null;

    /**
     * @var string
     */
    private $path_in_root = '';

    /**
     * @var null
     */
    private $new_name = null;

    /**
     * @var int
     */
    private $public = 0;

    /**
     * @param array $params
     */
    public function exec (array $params = array())
    {
        $this->path_in_root = self::$configs['file']['path'] .'/'.date('Y').'/'.date('m').'/'.date('d').'/'.date('H');
        $this->path = static::$app . (empty(static::$configs['file']['force']) ? static::$configs['app_name']. '/www' : '').  $this->path_in_root;
    }

    /**
     * @param array $file
     * @param array $return
     * @param int $public
     * @return array|string
     */
    public function upload (array $file = array(), $return=array(), $public=1)
    {
        $this->mkdir();

        $this->file_info($file);

        $this->public=$public;

        if (!in_array($this->ext,self::$configs['file']['ext']))
        {
            return array('error' => array('msg' => 'File extension not found in config', 'code' => 901));
        }

        if ($this->size > self::$configs['file']['max_size'])
        {
            return array('error' => array('msg' => 'File can\'t be more '.(self::$configs['file']['max_size']/10000).' mb', 'code' => 902));
        }

        $filename = $this->path . '/'.$this->new_name;
        if (copy($this->tmp_name,$filename))
        {
            $id = $this->add();
            $link = 'http://'.$_SERVER['HTTP_HOST'] . $this->path_in_root . '/' . $this->new_name;

            if ( !$return )
            {
                return $link;
            }
            else
            {
                if (in_array('id',$return))
                {
                    $return['id'] = $id;
                }

                if (in_array('name',$return))
                {
                    $return['name'] = $this->name;
                }

                if (in_array('new_name',$return))
                {
                    $return['new_name'] = $this->new_name;
                }

                if (in_array('ext',$return))
                {
                    $return['ext'] = $this->ext;
                }

                if (in_array('link',$return))
                {
                    $return['link'] = $link;
                }
                if (in_array('mime',$return))
                {
                    $return['mime'] = $link;
                }

                return $return;
            }
        }
    }

    /**
     * @param $file
     */
    private function file_info ($file)
    {
        $this->mime     = $file['type'];
        $this->name     = strtolower($file['name']);
        $this->ext      = strtolower( pathinfo ( $file['name'], PATHINFO_EXTENSION ) );
        $this->tmp_name = $file['tmp_name'];
        $this->size     = $file['size'];
        $this->hash     = md5_file($file['tmp_name']);
        $this->new_name = date('His').'_'.str_replace(' ', '_',$this->name);
    }

    /**
     *
     */
    private function mkdir ()
    {
        if ( !is_dir($this->path) )
        {
            mkdir($this->path,0777,true);
        }
    }

    /**
     * @return mixed
     */
    private function add ()
    {
        $this->create_table();
        $sql = "INSERT INTO `".self::$configs['file']['table']."`
                        (`name`,`title`,`hash`,`mime`,`size`,`path`,`user`,`ip`,`host`,`public`)
                VALUES
                        (
                          " . $this->mysqli->escape($this->name) . ",
                          " . $this->mysqli->escape($this->new_name) . ",
                          " . $this->mysqli->escape($this->hash) . ",
                          " . $this->mysqli->escape($this->mime) . ",
                          " . $this->mysqli->escape($this->size) . ",
                          " . $this->mysqli->escape($this->path_in_root) . ",
                          " . intval($this->sess->id) . ",
                          " . $this->mysqli->escape($_SERVER['REMOTE_ADDR']) . ",
                          " . $this->mysqli->escape($_SERVER['HTTP_HOST']) . ",
                          " . intval($this->public) . "
                        )";
        $this->mysqli->q($sql);
        return $this->mysqli->last_id();
    }

    /**
     *
     */
    private function create_table ()
    {
        $res = $this->mysqli->q("SHOW TABLES LIKE '" . self::$configs['file']['table'] . "'");

        if (!$this->mysqli->rows($res))
        {
            $sql = "CREATE TABLE `".self::$configs['file']['table']."`(
                        `id` INT(11) NOT NULL AUTO_INCREMENT,
                        `name` VARCHAR(655) NOT NULL,
                        `title` VARCHAR(655) NOT NULL,
                        `hash` VARCHAR(100) NOT NULL,
                        `mime` VARCHAR(100) NOT NULL,
                        `size` INT(11) NOT NULL,
                        `path` VARCHAR(655) NOT NULL,
                        `user` INT(11) NOT NULL,
                        `ip` VARCHAR(30) NOT NULL,
                        `dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `host` VARCHAR(128) NOT NULL,
                        `public` INT(11) NOT NULL,
                        PRIMARY KEY (`id`)
                    )
                    COLLATE='utf8_general_ci'
                    ENGINE=InnoDB;
                    ";
            $this->mysqli->q($sql);
        }
    }

    /**
     * @param null $id
     * @return bool
     */
    public function info($id=NULL)
    {
        if (!$id)
        {
            return false;
        }
        return $this->mysqli->fetch("SELECT * FROM file WHERE id=".intval($id));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Руслан
 * Date: 11.07.14
 * Time: 1:41
 */

namespace Yashr\Drivers\Mail;


class Smtp extends Email
{
    private $mail=null;
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function exec(array $params = array())
    {
        require_once self::$yashr."yashr/drivers/lib/smtp/class.phpmailer.php";
        if (is_null($this->mail))
        {
            $this->mail = new \PHPMailer;
            $this->mail->IsSMTP();
            $this->mail->Host = self::$configs['email']['smtp']['host']; //Hostname of the mail server
            $this->mail->Port = self::$configs['email']['smtp']['port']; //Port of the SMTP like to be 25, 80, 465 or 587
            $this->mail->SMTPAuth = true; //Whether to use SMTP authentication
            $this->mail->SMTPSecure = 'ssl';
            $this->mail->IsHTML(true);
            $this->mail->Username = self::$configs['email']['smtp']['user']; //Username for SMTP authentication any valid email created in your domain
            $this->mail->Password = self::$configs['email']['smtp']['pass']; //Password for SMTP authentication
            $this->mail->AddReplyTo(self::$configs['email']['smtp']['reply'], self::$configs['host']); //reply-to address
            $this->mail->SetFrom(self::$configs['email']['smtp']['from'], self::$configs['host']); //From address of the mail
        }
    }

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function send(array $params = array())
    {
        $default = array(
            'template'  => self::$configs['email']['template'],
            'user'      => 0,
            'to'        => 'inginer.r@gmail.com',
            'subject'   => 'no-reply',
            'data'      => array(

            ),
            'force' => false,
        );

        $params = array_merge($default,$params);

//        $id = $this->add($params);

//        if ($params['force'])
//        {
        $template = $this->view->display('email/' . $params['template'], $params['data'],true);
//        mail($params['to'],$params['subject'],$template);
        $this->mail->Subject = "Your SMTP Mail"; //Subject od your mail
        $this->mail->AddAddress($params['to'], self::$configs['host']); //To address who will receive this email
        $this->mail->MsgHTML($template); //Put your body of the message you can place html code here
        $this->mail->CharSet="UTF-8";
        $this->mail->Send(); //Send the mails
//        $this->mysqli->q("UPDATE ". self::$configs['email']['table'] ." SET status='done', `update`=NOW() WHERE id=" . intval($id));
//        }
    }
}
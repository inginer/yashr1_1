<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Mail;


use Yashr\Classes\Core;
use Yashr\Drivers\Mail\INF;

Core::import('yashr.drivers.view');

/**
 * Class Mail
 * @package Yashr\Drivers\Mail
 */
class Mail extends Email
{
    /**
     * @param array $params
     */
    public function exec(array $params = array()) { }

    /**
     * @param array $params
     */
    public function send(array $params = array())
    {
        $default = array(
            'template'  => self::$configs['email']['template'],
            'user'      => 0,
            'to'        => 'inginer.r@gmail.com',
            'subject'   => 'no-reply',
            'data'      => array(

            ),
            'force' => false,
        );

        $params = array_merge($default,$params);

        $id = $this->add($params);

        if ((self::$configs['email']['user'] != 'demon' && $id) || $params['force'])
        {
            $template = $this->view->display('email/' . $params['template'], $params['data'],true);
            mail($params['to'],$params['subject'],$template);
            $this->mysqli->q("UPDATE ". self::$configs['email']['table'] ." SET status='done', `update`=NOW() WHERE id=" . intval($id));
        }
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Mail;


use Yashr\Classes\Core;
use Yashr\Classes\Driver;

Core::import('yashr.drivers.db.mysqli');

/**
 * Class Email
 * @package Yashr\Drivers\Mail
 */
abstract class Email extends Driver
{
    /**
     * @param array $params
     * @return mixed
     */
    public abstract function send (array $params = array());

    /**
     * @param $params
     * @return mixed
     */
    protected function add ($params)
    {
        $this->create_table();

        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
        // Run the preg_match() function on regex against the email address
        if (preg_match($regex, $params['to']))
        {

            $this->mysqli->q("INSERT INTO " . self::$configs['email']['table'] . "
                        (`to`,data,template,subject,dt,user) VALUES
                        (
                        ". $this->mysqli->escape($params['to']).",
                        ". $this->mysqli->escape(json_encode($params['data']),true,false).",
                        ". $this->mysqli->escape($params['template']).",
                        ". $this->mysqli->escape($params['subject']).",
                        NOW(),
                        ". intval($params['user']) ."
                        )");
        }
        return $this->mysqli->last_id();

    }

    /**
     *
     */
    private function create_table ()
    {

        $res = $this->mysqli->q("SHOW TABLES LIKE '" . self::$configs['email']['table'] . "'");

        if (!$this->mysqli->rows($res))
        {
            $this->mysqli->q("CREATE TABLE " . self::$configs['email']['table'] . " (
                                `id` INT(10) NOT NULL AUTO_INCREMENT,
                                `to` VARCHAR(100) NOT NULL DEFAULT '0' COLLATE 'utf8_bin',
                                `status` ENUM('send','done') NOT NULL DEFAULT 'send' COLLATE 'utf8_bin',
                                `data` TEXT NOT NULL COLLATE 'utf8_bin',
                                `subject` VARCHAR(50) NOT NULL COLLATE 'utf8_bin',
                                `dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `update` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                                `user` INT(11) NOT NULL,
                                `template` VARCHAR(50) NOT NULL COLLATE 'utf8_bin',
                                PRIMARY KEY (`id`),
                                INDEX `to_user` (`to`, `user`)
                            )
                            COLLATE='utf8_bin'
                            ENGINE=InnoDB");
        }
    }
}
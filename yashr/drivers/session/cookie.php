<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 0:52
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Session;

use Yashr\Classes\Core;

Core::import('yashr.drivers.hash');

/**
 * Class Cookie
 * @package Yashr\Drivers\Session
 */
class Cookie extends INF
{
    /**
     * @var null
     */
    private $hash = null;

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->hash = self::call('hash');
    }

    /**
     * @param array $params
     */
    public function set(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            $t = self::$configs['cookie']['time'];
            $v = $val;

            if (is_array($val) && !empty($val))
            {
                $v = $val[0];
                $t = $val[1];
            }
            setcookie($key, $this->hash->encode($v),time()+$t,self::$configs['cookie']['path']);
        }
    }

    /**
     * @param $get
     * @return null
     */
    public function get($get)
    {
        return (isset($_COOKIE[$get]) ?  $this->hash->decode($_COOKIE[$get]) : null);

    }

    /**
     * @param array $params
     */
    public function clean(array $params = array())
    {
        foreach ($params as $val)
        {
            setcookie($val, null, -1,self::$configs['cookie']['path']);
        }
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 0:52
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Session;


use Yashr\Classes\Driver;

/**
 * Class INF
 * @package Yashr\Drivers\Session
 */
abstract class INF extends Driver
{
    /**
     * @param array $params
     * @return mixed
     */
    public abstract function set (array $params = array());

    /**
     * @param $get
     * @return mixed
     */
    public abstract function get ( $get );

    /**
     * @param $name
     * @param $val
     */
    public function __set ($name,$val)
    {
        $this->set(array($name => $val));
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get ($name)
    {
        return $this->get($name);
    }
}
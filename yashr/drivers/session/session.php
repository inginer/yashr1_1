<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 0:51
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Session;

use Yashr\Classes\Core;

Core::import('yashr.drivers.hash');

/**
 * Class Session
 * @package Yashr\Drivers\Session
 */
class Session extends INF
{
    /**
     * @var null
     */
    private $hash = null;

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->hash = self::call('hash');
    }

    /**
     * @param array $params
     */
    public function set(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            $_SESSION[$key] = $this->hash->encode($val);
        }
    }

    /**
     * @param $get
     * @return null
     */
    public function get($get)
    {
        return (isset($_SESSION[$get]) ?  $this->hash->decode($_SESSION[$get]) : null);
    }

    /**
     * @param array $params
     */
    public function clean(array $params = array())
    {
        foreach ($params as $val)
        {
            unset($_SESSION[$val]);
        }
    }

}
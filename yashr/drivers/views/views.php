<?php
/**
 * Created by PhpStorm.
 * User: Ruslan Madatov
 * Date: 31.12.13
 * Time: 0:29
 */

namespace Yashr\Drivers\views;


use Yashr\Classes\Driver;

/**
 * Class Views
 * @package Yashr\Drivers\views
 */

abstract class Views extends Driver
{
    /**
     * @var array
     */
    protected $data = array();

    /**
     * @var string
     */
    protected $file = '';

    /**
     * @var string
     */
    protected $layout_file = '';

    /**
     * @var string
     */
    protected $view_path = '';

    /**
     * @var string
     */
    protected $html = '';
    /**
     * @var bool
     */
    protected $cache = false;
    /**
     * @var int
     */
    protected $cache_life_time = 0;

    /**
     * @param $file
     * @param array $data
     * @param bool $return
     * @return mixed
     */
    abstract public function display ($file, $data=array(), $return=false);

    /**
     * @param array $params
     */
    public function assign (array $params=array())
    {
        $this->data=array_merge($this->data, $params);
    }
}
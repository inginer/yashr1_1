<?php
/**
 * Created by PhpStorm.
 * User: Ruslan Madatov
 * Date: 31.12.13
 * Time: 0:35
 */

namespace Yashr\Drivers\views;


use Yashr\Classes\Core;
use Yashr\Classes\YashrException;

Core::import('yashr.drivers.db.mongo');

class Yview extends Views
{
    /**
     * @param array $params
     * @return mixed
     */
    public function exec(array $params = array())
    {
        $this->view_path = static::$app.static::$configs['app_name'].'/views/';
    }

    /**
     * @param $file
     * @param array $data
     * @param bool $return
     * @return mixed
     */
    public function display($file, $data = array(), $return = false)
    {
        $file=$this->view_path.$file.static::$ext;

        if (!is_file($file))
        {
            throw new YashrException("File: ".$file." not found!");
        }

        extract($this->data);

        ob_start();
        ob_implicit_flush(false);

        require($file);

        $content = ob_get_clean();

        if ($this->cache)
        {
            $cache_file_nme=md5($file);

            $q=$this->mongo->q("templates");

            $q->insert(array(
                'id'        => $cache_file_nme,
                'data'      => $content,
                'date'      => time(),
                'delete'    => time()+$this->cache_life_time,
            ));
        }

        extract($this->data);
    }

    public function set_layout ($layout_file='')
    {
        if ($layout_file)
        {
            $this->layout_file=$layout_file;
        }
    }
}
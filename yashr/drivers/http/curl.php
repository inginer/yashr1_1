<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 13.09.13
 * Time: 12:52
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Http;

use Yashr\Classes\Driver;

class Curl extends Driver
{
    private $method='POST';
    private $url='';
    private $ssl=false;
    private $return=true;
    private $params=array();

    public function exec(array $params = array())
    {
    }

    public function connect ($url,$params=array(),$method='POST',$ssl=false)
    {
        $this->url($url)->set_method($method)->set_ssl($ssl);
        $curl=curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->url);

        if ($method=='POST')
        {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $out = curl_exec($curl);
        curl_close($curl);

        return $out;
    }

    public function set_method($method)
    {
        $this->method=$method;
        return $this;
    }

    public function set_ssl($ssl)
    {
        $this->ssl=$ssl;
        return $this;
    }

    public function url ($url)
    {
        $this->url=($this->ssl ? 'https' : 'http').'://'.$url;
        return $this;
    }

    public function set_params ($params)
    {


        $this->params=$params;
        return $this;
    }
}
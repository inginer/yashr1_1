<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers;


use Yashr\Classes\Core;
use Yashr\Classes\Driver;
use Yashr\Classes\YashrException;
include_once('lib/codecompressor.class.php');
Core::import('yashr.drivers.cache.mem');

/**
 * Class View
 * @deprecated
 * @package Yashr\Drivers
 */
class View extends Driver
{
    /**
     * @var array
     */
    private $data = array();
    /**
     * @var string
     */
    private $layout = 'layout/main';
    /**
     * @var string
     */
    private $template='default';
    /**
     * @var string
     */
    private $cache_prefix='template_view';

    /**
     * @var bool
     */
    public $cache=false;
    /**
     * @var bool
     */
    public $cacheWithLayout=false;
    /**
     * @var int
     */
    public $cache_life_time=10;

    /**
     * @param $file
     * @param array $params
     * @param bool $return
     * @return bool|string
     * @throws \Yashr\Classes\YashrException
     */
    public function display ( $file, $params = array(), $return = false)
    {
        $file_name = $this->template.'/'.$file.static::$ext;

        if (!is_file($file_name))
        {
            throw new YashrException('The template file not found! ' . $file_name);
        }
        $cache_file_nme=md5($file_name);

        $content="";

        if ($this->cache)
        {
            $content=$this->mem->get($cache_file_nme);
        }

        if ( !$this->cache || !$content )
        {
            $this->assign($params);

            extract($this->data);

            ob_start();
            ob_implicit_flush(false);

            require($file_name);

//            $content = ob_get_clean();
            $content = \CodeCompressor::compressHtml(ob_get_clean());

            if ($this->cache)
            {
                $this->mem->set($cache_file_nme, $content, $this->cache_life_time);
            }

        }

        if ($return)
        {
            return $content;
        }
        else
        {
            echo $this->display($this->layout, $this->data + array('content' => $content), true);
        }
        return false;
    }

    /**
     * @param array $params
     */
    public function assign (array $params = array())
    {
        $this->data = array_merge($this->data,$params);
    }

    /**
     * @param null $layout
     */
    public function set_layout ($layout = null)
    {
        if (!is_null($layout))
        {
            $this->layout = $layout;
        }
    }

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->set_template_path(self::$configs['template']);
    }

    /**
     * @param $path
     */
    public function set_template_path ($path)
    {
        $this->template=static::$app.static::$configs['app_name'].'/views'.self::$configs['templates'][$path]['path'];
    }
}
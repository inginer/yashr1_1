<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\DB;

class PDO extends Db
{
    /**
     * @param array $params
     * @return mixed
     */
    public function exec(array $params = array())
    {
        // TODO: Implement exec() method.
    }

    /**
     * @return mixed
     */
    protected function connect()
    {
        // TODO: Implement connect() method.
    }

    /**
     * @return mixed
     */
    protected function close()
    {
        // TODO: Implement close() method.
    }

    /**
     * @param $sql
     * @param bool $master
     * @return mixed
     */
    public function q($sql, $master = true)
    {
        // TODO: Implement q() method.
    }

    /**
     * @param $query
     * @return mixed
     */
    public function fetch($query)
    {
        // TODO: Implement fetch() method.
    }

    /**
     * @param $query
     * @return mixed
     */
    public function fetch_all($query)
    {
        // TODO: Implement fetch_all() method.
    }

    /**
     * @return mixed
     */
    public function last_id()
    {
        // TODO: Implement last_id() method.
    }

    /**
     * @param $query
     * @return mixed
     */
    public function rows($query)
    {
        // TODO: Implement rows() method.
    }

    /**
     * @return mixed
     */
    public function affected_rows()
    {
        // TODO: Implement affected_rows() method.
    }

    /**
     * @param $val
     * @param bool $trim
     * @return mixed
     */
    public function escape($val, $trim = true)
    {
        // TODO: Implement escape() method.
    }

    /**
     * @return mixed
     */
    public function transaction()
    {
        // TODO: Implement transaction() method.
    }

    /**
     * @return mixed
     */
    public function commit()
    {
        // TODO: Implement commit() method.
    }

    /**
     * @return mixed
     */
    public function rollback()
    {
        // TODO: Implement rollback() method.
    }
}
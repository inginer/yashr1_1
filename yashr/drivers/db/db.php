<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\DB;


use Yashr\Classes\Driver;

/**
 * Class Db
 * @package Yashr\Drivers\DB
 */
abstract class Db extends Driver
{
    /**
     * @var null
     */
    protected $db = null;

    /**
     * @return mixed
     */
    protected abstract function connect ();

    /**
     * @return mixed
     */
    protected abstract function close ();

    /**
     * @param $sql
     * @param bool $master
     * @return mixed
     */
    public abstract function q ( $sql, $master = true );

    /**
     * @param $query
     * @return mixed
     */
    public abstract function fetch ( $query );

    /**
     * @param $query
     * @return mixed
     */
    public abstract function fetch_all ( $query );

    /**
     * @return mixed
     */
    public abstract function last_id ();

    /**
     * @param $query
     * @return mixed
     */
    public abstract function rows ( $query );

    /**
     * @return mixed
     */
    public abstract function affected_rows () ;

    /**
     * @param $val
     * @param bool $trim
     * @return mixed
     */
    public abstract function escape ( $val, $trim=true );

    /**
     * @return mixed
     */
    public abstract function transaction ();

    /**
     * @return mixed
     */
    public abstract function commit ();

    /**
     * @return mixed
     */
    public abstract function rollback ();

    /**
     * @param $str
     * @param bool $trim
     * @return mixed
     */
    public function filt ($str, $trim=true)
    {
        if ($trim)
        {
            $str = trim($str);
        }
        $from = array("'", '"', '>', '<', '`', '\\');
        $to = array("&#039;", "&#34;", "&#62;", "&#60;", "&#96;", "&#92;");
        return str_replace($from, $to, $str);
    }

/*
    public function follow_query_table ()
    {
        $sql = "CREATE TABLE ".self::$configs['db']['follow_query']['table']." (
                    `hash` CHAR(32) NOT NULL COLLATE 'utf8_bin',
                    `query` TEXT NOT NULL COLLATE 'utf8_bin',
                    `type` CHAR(50) NOT NULL COLLATE 'utf8_bin',
                    `last_query` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    `memory` INT(11) NOT NULL,
                    `time` CHAR(50) NOT NULL COLLATE 'utf8_bin',
                    `count` INT(11) NOT NULL,
                    UNIQUE INDEX `mysql_hash` (`hash`),
                    INDEX `hash_query` (`hash`),
                    INDEX `time` (`time`),
                    INDEX `count` (`count`)
                )
                COLLATE='utf8_bin'
                ENGINE=InnoDB";
        return $sql;
    }
*/
}

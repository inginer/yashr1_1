<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 6/16/13
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\DB;
use Yashr\Classes\YashrException;


/**
 * Class Mongo
 * @package Yashr\Drivers\DB
 */
class Mongo extends Db
{
    /**
     * @var null
     */
    private $db_cursor=null;
    /**
     * @var null
     */
    private $cursor=null;

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->connect();
    }

    /**
     *
     */
    protected function connect()
    {
        if (class_exists('MongoClient'))
        {
            $this->db = new \MongoClient(
                "mongodb://".
                static::$configs['db']['mongo']['host'].":".
                static::$configs['db']['mongo']['port']
            );

            $this->db_cursor = $this->db->selectDB(
                static::$configs['db']['mongo']['dbname']
            );
        }
        else
        {
            throw new YashrException('Mongo extension not found!');
        }
    }

    /**
     *
     */
    protected function close()
    {
        $this->db->close($this->db);
    }

    /**
     * @param $collection
     * @param bool $master
     * @return null
     */
    public function q($collection, $master = true)
    {
        $this->cursor = $this->db_cursor->selectCollection($collection);

        return $this->cursor;
    }

    /**
     * @param array $query
     * @return mixed
     */
    public function fetch($query=array('params' => array(),'col' => array()))
    {
        return $this->cursor->findOne($query['params'],$query['col']);
    }

    /**
     * @param array $query
     * @return mixed
     */
    public function fetch_all($query=array('params' => array(),'col' => array()))
    {
        return $this->cursor->find($query['params'],$query['col']);
    }

    /**
     *
     */
    public function last_id()
    {

    }

    /**
     * @param $query
     */
    public function rows($query)
    {

    }

    /**
     * @param $val
     * @param bool $trim
     */
    public function escape($val, $trim = true)
    {

    }

    /**
     *
     */
    public function transaction()
    {}

    /**
     *
     */
    public function commit()
    {}

    /**
     *
     */
    public function rollback()
    {}

    /**
     * @param string $charset
     */
    public function charset($charset = "utf-8")
    {}

    /**
     * @param string $charset
     */
    public function time_zone($charset = "Europe/Moscow")
    {}

    /**
     *
     */
    public function affected_rows()
    {
        // TODO: Implement affected_rows() method.
    }
}
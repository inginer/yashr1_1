<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\DB;

use Yashr\Classes\YashrException;

/**
 * Class MySQLi
 * @package Yashr\Drivers\DB
 */
class MySQLi extends Db
{
    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->connect();
    }

    /**
     * @throws \Yashr\Classes\YashrException
     */
    protected function connect()
    {
        $this->db = mysqli_connect(self::$configs['db']['host'],self::$configs['db']['user'],self::$configs['db']['pass'],self::$configs['db']['dbname']);

        if (!$this->db)
        {
            throw new YashrException('Sorry we cant connect with server');
        }
        mysqli_query($this->db,"set names '".self::$configs['db']['charset']."'");
        mysqli_query($this->db,"set time_zone='".self::$configs['db']['timezone']."'");
    }

    /**
     *
     */
    protected function close()
    {
        mysqli_close($this->db);
    }

    /**
     * @param $sql
     * @param bool $master
     * @return bool|\mysqli_result
     * @throws \Yashr\Classes\YashrException
     */
    public function q($sql, $master = true)
    {
        $query = mysqli_query($this->db,$sql);

        if (mysqli_error($this->db))
        {
            throw new YashrException('Query error => ' .mysqli_error($this->db) . ' Your sql: ' . $sql);
        }

        return $query;
    }

    /**
     * @param $sql
     * @return array|null
     */
    public function fetch ($sql)
    {
        if (is_string($sql))
        {
            $sql = $this->q($sql);
        }

        return mysqli_fetch_assoc($sql);
    }

    /**
     * @param $sql
     * @return array
     */
    public function fetch_all ($sql)
    {
        $aReturn = array();

        if (self::$configs['debug'])
        {
            $aReturn['sql'] = $sql;
        }

        if (is_string($sql))
        {
            $sql = $this->q($sql);
        }

        $aReturn['counter'] = $this->rows($sql);

        while ($row = $this->fetch($sql))
        {
            $aReturn['data'][] = $row;
        }

        $count=$this->result("SELECT FOUND_ROWS() AS rows",'rows');
        $aReturn['count']=$count;

        return $aReturn;
    }

    /**
     * @param $sql
     * @param $row
     * @return bool
     */
    public function result ($sql, $row)
    {
        if (is_string($sql))
        {
            $sql = $this->fetch($sql);
        }

        return (!empty($sql[$row]) ? $sql[$row] : false);
    }

    /**
     * @return array|int|null|string
     */
    public function last_id()
    {
        $id = mysqli_insert_id($this->db);
        if (!$id)
        {
            $id = $this->fetch("SELECT LAST_INSERT_ID() as id");
            $id = $id['id'];
        }
        return $id;
    }

    /**
     *
     */
    public function transaction ()
    {
        mysqli_autocommit($this->db,false);
    }

    /**
     *
     */
    public function commit()
    {
        mysqli_commit($this->db);
        mysqli_autocommit($this->db,true);
    }

    /**
     *
     */
    public function rollback ()
    {
        mysqli_rollback($this->db);
        mysqli_autocommit($this->db,true);
    }

    /**
     * @param $sql
     * @return int
     */
    public function rows ( $sql )
    {
        if (is_string($sql))
        {
            $sql = $this->q($sql);
        }

        return mysqli_num_rows($sql);
    }

    /**
     * @param $var
     * @param bool $trim
     * @param bool $chars
     * @return string
     */
    public function escape ($var, $trim = true, $chars=true)
    {
        if ($chars)
        {
            $var=$this->filt($var);
        }

        $trim = (($trim) ? "'" : "");
        return $trim . mysqli_real_escape_string($this->db,$var) . $trim;
    }

    /**
     * @return int
     */
    public function affected_rows()
    {
        return mysqli_affected_rows($this->db);
    }
}
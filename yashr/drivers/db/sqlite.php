<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 6/1/13
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\DB;


use Yashr\Classes\YashrException;

/**
 * Class Sqlite
 * @package Yashr\Drivers\DB
 */
class Sqlite extends Db
{

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->connect();
    }

    /**
     * @throws \Yashr\Classes\YashrException
     */
    protected function connect()
    {
        $this->db = sqlite_open(self::$configs['db']['sqlite']['file'], self::$configs['db']['sqlite']['mode']);

        if (!$this->db)
        {
            throw new YashrException('Sorry we can\'t open sqlite base file');
        }

        sqlite_query($this->db,"SET NAMES '".self::$configs['db']['sqlite']['charset']."'");
    }

    /**
     *
     */
    protected function close()
    {

    }

    /**
     * @param $sql
     * @param bool $master
     * @return \SQLiteResult
     * @throws \Yashr\Classes\YashrException
     */
    public function q($sql, $master = true)
    {
        $query = sqlite_query($this->db,$sql);

        if (sqlite_last_error($this->db))
        {
            throw new YashrException('Query error => ' . sqlite_error_string ( sqlite_last_error($this->db) ) . ' Your sql: ' . $sql);
        }

        return $query;
    }

    /**
     * @param $query
     */
    public function fetch($query)
    {

    }

    /**
     * @param $query
     */
    public function fetch_all($query)
    {
        // TODO: Implement fetch_all() method.
    }

    /**
     *
     */
    public function last_id()
    {
        // TODO: Implement last_id() method.
    }

    /**
     * @param $query
     */
    public function rows($query)
    {
        // TODO: Implement rows() method.
    }

    /**
     * @param $val
     * @param bool $trim
     */
    public function escape($val, $trim = true)
    {
        // TODO: Implement escape() method.
    }

    /**
     *
     */
    public function transaction()
    {
        // TODO: Implement transaction() method.
    }

    /**
     *
     */
    public function commit()
    {
        // TODO: Implement commit() method.
    }

    /**
     *
     */
    public function rollback()
    {
        // TODO: Implement rollback() method.
    }

    /**
     * @param string $charset
     */
    public function charset($charset = "utf-8")
    {
        // TODO: Implement charset() method.
    }

    /**
     * @param string $charset
     */
    public function time_zone($charset = "Europe/Moscow")
    {
        // TODO: Implement time_zone() method.
    }

    /**
     *
     */
    public function affected_rows()
    {
        // TODO: Implement affected_rows() method.
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 11.05.13
 * Time: 2:38
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Cache;


/**
 * Class Mem
 * @package Yashr\Drivers\Cache
 */
class Mem extends Cache
{
    /**
     * @var null
     */
    private $mem = NULL;

    /**
     * @param array $params
     */
    public function exec(array $params = array())
    {
        $this->connect();
        $this->addServer();
    }

    /**
     * @return bool|null|void
     */
    protected function connect ()
    {
        if (!function_exists('memcache_connect'))
        {
            return false;
        }

        if (is_null($this->mem))
        {
            $this->mem = memcache_connect(self::$configs['mem']['host'], self::$configs['mem']['port']);
        }
        return $this->mem;
    }

    /**
     * @return bool|null
     */
    private function addServer()
    {
        if (!function_exists('memcache_add_servers'))
        {
            return false;
        }

        if (count(self::$configs['mem']['servers']))
        {
            foreach (self::$configs['mem']['servers'] as $server)
            {
                if (!$server['connect'])
                {
                    continue;
                }
                memcache_add_server($this->mem,$server['host'],$server['port']);
            }
        }
        return $this->mem;
    }

    /**
     * @param $key
     * @param $val
     * @param int $timeout
     * @return bool
     */
    public function set ($key, $val, $timeout=86400)
    {
        if (!function_exists('memcache_set'))
        {
            return false;
        }

        if (!is_array($key))
        {
            memcache_set($this->mem, $key, $val, MEMCACHE_COMPRESSED, $timeout);
        }
        else
        {
            foreach ($key as $k => $v)
            {
                memcache_set($this->mem, $k, $v, MEMCACHE_COMPRESSED, $val);
            }
        }
        return (!$val ? $val : true);
    }

    /**
     * @param $key
     * @return bool|null|void
     */
    public function get ($key)
    {
        if (!function_exists('memcache_get'))
        {
            return false;
        }

        $var = memcache_get($this->mem, $key);

        if (!$var)
        {
            return null;
        }

        return $var;
    }

    /**
     * @param array $params
     * @return null|void
     */
    public function clear (array $params = array())
    {
        if (empty($params))
        {
            return memcache_flush($this->mem);
        }

        foreach ($params as $key)
        {
            memcache_delete($this->mem,$key);
        }
        return $this->mem;
    }

    /**
     * @param $key
     * @return bool|null|void
     */
    public function __get ($key)
    {
        return $this->get($key);
    }

    /**
     * @param $key
     * @param $val
     * @return bool
     */
    public function __set($key,$val)
    {
        $timeout = 86400;
        $var = $val;

        if (is_array($val))
        {
            list($var, $timeout) = $val;
        }

        return $this->set($key, $var, $timeout);
    }
}
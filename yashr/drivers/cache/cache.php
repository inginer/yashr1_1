<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Руслан
 * Date: 10.05.13
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Cache;


use Yashr\Classes\Driver;

/**
 * Class INF
 * @package Yashr\Drivers\Cache
 */
abstract class Cache extends Driver
{
    /**
     * @return mixed
     */
    protected abstract function connect() ;

    /**
     * @param $key
     * @param $val
     * @param int $time
     * @return mixed
     */
    public abstract function set ( $key, $val, $time = 86400 );

    /**
     * @param $key
     * @return mixed
     */
    public abstract function get ( $key );

    /**
     * @param array $keys
     * @return mixed
     */
    public abstract function clear (array $keys=array() );

}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inginer
 * Date: 22.09.13
 * Time: 3:31
 * To change this template use File | Settings | File Templates.
 */

namespace Yashr\Drivers\Http2;


use Yashr\Classes\Driver;

/**
 * Class Http2
 * @package Yashr\Drivers\Http2
 */
abstract class Http2 extends Driver
{
    /**
     * @param $url
     * @return mixed
     */
    abstract public function connect ($url);

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function request (array $data=array());
}
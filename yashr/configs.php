<?php

return array(
    'email'     => array(
        'user'      => 'demon',
        'table'     => 'email',
        'template'  => 'default',
        'smtp'      => array(
            'host'  => '',
            'port'  => '',
            'user'  => '',
            'pass'  => '',
            'from'  => '',
            'reply'  => '',
        ),
    ),
    'error'     => array(
        'table'     => 'error',
        'level'     => E_ALL,
        'template'  => 'default',
    ),
    'hash'      => '69dab461d1b77e317c4be3a45d154dbc',
    'cookie'    => array(
        'time' => 86400,
        'path' => '/',
    ),
    'file' => array(
        'path'  => '/up',
        'ext'   => array(
            'jpg',
            'jpeg',
            'png',
            'gif',
            'xlsx',
        ),
        'max_size' => 20000000,
        'table' => 'file',
    ),
    'access_ip' => array(
    ),
    'debug' => true,
    'template'  => 'default',
    'templates' => array(
        'default' => '',
        'metro'   => array('path'=>'/metro'),
    ),
    'session' => true,
);